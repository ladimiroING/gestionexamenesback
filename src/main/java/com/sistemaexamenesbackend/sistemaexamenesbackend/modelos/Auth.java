package com.sistemaexamenesbackend.sistemaexamenesbackend.modelos;

import org.springframework.security.core.GrantedAuthority;

public class Auth implements GrantedAuthority{

    private String authority;

    
    public Auth(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        
        return this.authority;
    }
    
}
