package com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Rol;

public interface RolRepo extends JpaRepository<Rol, Long>{
    
}
