package com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Usuario;

public interface UsuarioRepo extends JpaRepository<Usuario, Long>{
    
    public Usuario findByUsername(String username);
}
