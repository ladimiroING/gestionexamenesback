package com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Examen;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Pregunta;

public interface PreguntaRepo extends JpaRepository<Pregunta, Long>{
    
    Set<Pregunta> findByExamen(Examen examen);
}
