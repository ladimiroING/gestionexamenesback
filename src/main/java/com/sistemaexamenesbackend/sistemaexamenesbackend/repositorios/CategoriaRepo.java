package com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Categoria;

public interface CategoriaRepo extends JpaRepository<Categoria, Long>{
    
}
