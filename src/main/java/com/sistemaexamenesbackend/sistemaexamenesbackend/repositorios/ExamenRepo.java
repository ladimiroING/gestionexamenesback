package com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Categoria;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Examen;

public interface ExamenRepo extends JpaRepository<Examen, Long>{
    
    List<Examen> findByCategoria(Categoria categoria);

    List<Examen> findByActivo(Boolean estado);

    List<Examen> findByCategoriaAndActivo(Categoria categoria,Boolean estado);
}
