package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios;

import java.util.Set;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Examen;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Pregunta;

public interface PreguntaServicio {
    
    Pregunta agregarPregunta(Pregunta pregunta);

    Pregunta actualizarPregunta(Pregunta pregunta);

    Set<Pregunta> obtenerPreguntas();

    Pregunta obtenerPregunta(Long preguntaId);

    Set<Pregunta> obtenerPreguntasDelExamen(Examen examen);

    void eliminarPregunta(Long preguntaId);

    Pregunta listarPregunta(Long preguntaId);
}
