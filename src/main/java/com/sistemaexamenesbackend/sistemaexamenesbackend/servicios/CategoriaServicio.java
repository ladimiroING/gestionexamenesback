package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios;

import java.util.Set;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Categoria;

public interface CategoriaServicio {
    
    Categoria agregarCategoria(Categoria categoria);

    Categoria actualizarCategoria(Categoria categoria);

    Set<Categoria> obtenerCategorias();

    Categoria obtenerCategoria(Long categoriaId);

    void eliminarCategoria(Long categoriaId);
}
