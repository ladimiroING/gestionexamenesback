package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios;

import java.util.Set;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Usuario;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.UsuarioRol;

public interface UsuarioServicio {
    
    public Usuario guardarUsuario(Usuario usuario, Set<UsuarioRol> usuarioRoles) throws Exception;

    public Usuario obtenerUsuario(String username);

    public void eliminarUsuario(Long usuarioId);
}
