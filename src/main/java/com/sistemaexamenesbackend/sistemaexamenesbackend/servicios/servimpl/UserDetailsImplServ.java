package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios.servimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Usuario;
import com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios.UsuarioRepo;

@Service
public class UserDetailsImplServ implements UserDetailsService{

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        Usuario usuario = this.usuarioRepo.findByUsername(username);
        if(usuario == null){
            throw new UsernameNotFoundException("Usuario no encontrado");
        }

        return usuario;
    }
    
}
