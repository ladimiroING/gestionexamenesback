package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios.servimpl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistemaexamenesbackend.sistemaexamenesbackend.exceptions.UserFoundException;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Usuario;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.UsuarioRol;
import com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios.RolRepo;
import com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios.UsuarioRepo;
import com.sistemaexamenesbackend.sistemaexamenesbackend.servicios.UsuarioServicio;


@Service
public class UsuarioServImpl implements UsuarioServicio{

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Autowired
    private RolRepo rolRepo;
    
    @Override
    public Usuario guardarUsuario(Usuario usuario, Set<UsuarioRol> usuarioRoles) throws Exception {
        Usuario usuarioLocal = usuarioRepo.findByUsername(usuario.getUsername());
        if(usuarioLocal != null){
            //System.out.println("El usuario ya existe");
            throw new UserFoundException("El usuario ya esta presente");
        }
        else{
            for(UsuarioRol usuarioRol:usuarioRoles){
                rolRepo.save(usuarioRol.getRol());
            }
            usuario.getUsuarioRoles().addAll(usuarioRoles);
            usuarioLocal = usuarioRepo.save(usuario);
        }
        return usuarioLocal;
    }

    @Override
    public Usuario obtenerUsuario(String username) {
        return usuarioRepo.findByUsername(username);
    }

    @Override
    public void eliminarUsuario(Long usuarioId) {
        usuarioRepo.deleteById(usuarioId);
        
    }
    
}
