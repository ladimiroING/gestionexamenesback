package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios.servimpl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Examen;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Pregunta;
import com.sistemaexamenesbackend.sistemaexamenesbackend.repositorios.PreguntaRepo;
import com.sistemaexamenesbackend.sistemaexamenesbackend.servicios.PreguntaServicio;

@Service
public class PreguntaServImpl implements PreguntaServicio{

    @Autowired
    private PreguntaRepo preguntaRepository;

    @Override
    public Pregunta agregarPregunta(Pregunta pregunta) {
        return preguntaRepository.save(pregunta);
    }

    @Override
    public Pregunta actualizarPregunta(Pregunta pregunta) {
        return preguntaRepository.save(pregunta);
    }

    @Override
    public Set<Pregunta> obtenerPreguntas() {
        return (Set<Pregunta>) preguntaRepository.findAll();
    }

    @Override
    public Pregunta obtenerPregunta(Long preguntaId) {
        return preguntaRepository.findById(preguntaId).get();
    }

    @Override
    public Set<Pregunta> obtenerPreguntasDelExamen(Examen examen) {
        return preguntaRepository.findByExamen(examen);
    }

    @Override
    public void eliminarPregunta(Long preguntaId) {
        Pregunta pregunta = new Pregunta();
        pregunta.setPreguntaId(preguntaId);
        preguntaRepository.delete(pregunta);
    }

    @Override
    public Pregunta listarPregunta(Long preguntaId) {
        return this.preguntaRepository.getOne(preguntaId);
    }    
}
