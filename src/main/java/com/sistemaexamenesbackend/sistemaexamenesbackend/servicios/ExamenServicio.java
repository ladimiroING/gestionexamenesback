package com.sistemaexamenesbackend.sistemaexamenesbackend.servicios;

import java.util.*;

import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Categoria;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Examen;

public interface ExamenServicio {
    
    Examen agregarExamen(Examen examen);

    Examen actualizarExamen(Examen examen);

    Set<Examen> obtenerExamenes();

    Examen obtenerExamen(Long examenId);

    void eliminarExamen(Long examenId);

    List<Examen> listarExamenesDeUnaCategoria(Categoria categoria);

    List<Examen> obtenerExamenesActivos();

    List<Examen> obtenerExamenesActivosDeUnaCategoria(Categoria categoria);
}
