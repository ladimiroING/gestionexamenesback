package com.sistemaexamenesbackend.sistemaexamenesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaexamenesbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaexamenesbackendApplication.class, args);
	}

}
