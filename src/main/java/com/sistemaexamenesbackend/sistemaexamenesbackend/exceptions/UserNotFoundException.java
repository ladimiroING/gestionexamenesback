package com.sistemaexamenesbackend.sistemaexamenesbackend.exceptions;

public class UserNotFoundException extends Exception{
    
    public UserNotFoundException(){
        super("El usuario con ese username no existe, vuelva a probar con otro!");
    }

    public UserNotFoundException(String msn){
        super(msn);
    } 
}
