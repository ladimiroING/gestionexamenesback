package com.sistemaexamenesbackend.sistemaexamenesbackend.exceptions;

public class UserFoundException extends Exception{
    
    public UserFoundException(){
        super("El usuario con ese username ya existe, vuelva a probar con otro!");
    }

    public UserFoundException(String msn){
        super(msn);
    }
}
