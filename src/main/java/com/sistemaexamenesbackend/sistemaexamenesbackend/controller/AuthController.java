package com.sistemaexamenesbackend.sistemaexamenesbackend.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sistemaexamenesbackend.sistemaexamenesbackend.config.AuthManager;
import com.sistemaexamenesbackend.sistemaexamenesbackend.config.JwtUtils;
import com.sistemaexamenesbackend.sistemaexamenesbackend.exceptions.UserNotFoundException;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.JwtRequest;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.JwtResponse;
import com.sistemaexamenesbackend.sistemaexamenesbackend.modelos.Usuario;
import com.sistemaexamenesbackend.sistemaexamenesbackend.servicios.servimpl.UserDetailsImplServ;

@RestController
@CrossOrigin("*")
public class AuthController {
    
    
    @Autowired
    private AuthManager authManager;

    @Autowired
    private UserDetailsImplServ userDetailsService;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/generate-token")
    public ResponseEntity<?> generarToken(@RequestBody JwtRequest jwtRequest) throws Exception {
        try{
            autenticar(jwtRequest.getUsername(),jwtRequest.getPassword());
        }catch (UserNotFoundException exception){
            exception.printStackTrace();
            throw new Exception("Usuario no encontrado");
        }

        UserDetails userDetails =  this.userDetailsService.loadUserByUsername(jwtRequest.getUsername());
        String token = this.jwtUtils.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void autenticar(String username,String password) throws Exception {
        try{
            authManager.authenticate(new UsernamePasswordAuthenticationToken(username,password));
        }catch (DisabledException exception){
            throw  new Exception("USUARIO DESHABILITADO " + exception.getMessage());
        }catch (BadCredentialsException e){
            throw  new Exception("Credenciales invalidas " + e.getMessage());
        }
    }

    @GetMapping("/actual-usuario")
    public Usuario obtenerUsuarioActual(Principal principal){
        return (Usuario) this.userDetailsService.loadUserByUsername(principal.getName());
    }

}
